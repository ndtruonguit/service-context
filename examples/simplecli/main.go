package main

import (
	"gitlab.com/ndtruonguit/service-context/examples/simplecli/cmd"
)

func main() {
	cmd.Execute()
}
